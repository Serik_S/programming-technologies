﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLASS
{
    class Car
    {
        private string color;
        public Car(string _color)
        {
            color = _color;
        }
        public override string ToString()
        {
            return color.ToString();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Car a = new Car("green");
            Console.WriteLine(a);
            Console.ReadKey();
        }
    }
}