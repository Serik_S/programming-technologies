﻿using System;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Line
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int x = 190;
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Pen pen = new Pen(Color.Black, 1);
            e.Graphics.DrawLine(pen, x, 200, x, 400);
        }
        private void button1_Click(object sender, System.EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);

            Pen pen = new Pen(Color.Black, 1);
            g.DrawLine(pen, x += 10, 200, x, 400);
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);

            Pen pen = new Pen(Color.Black, 1);
            g.DrawLine(pen, x -= 10, 200, x, 400);
        }
    }
}

