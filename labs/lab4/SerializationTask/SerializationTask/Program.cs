﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SerializationTask
{
    public enum Genders: int { male, female };

    
    public class Student
    {
        public string name;
        public string surname;
        [XmlIgnore]
        public int age;
        public Genders gender;
        public Student()
        {

        }
        public Student (string _name, string _surname, int _age, Genders _gender){
            name = _name;
            surname = _surname;
            age = _age;
            gender = _gender;
        }
        public override string ToString()
{
 	 return name + " " + surname + " " + age + " " + gender;
}
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student a  = new Student("Serik", "Seidigalimov", 18, Genders.male);
            XmlSerializer p = new XmlSerializer(typeof(Student));
            StreamWriter fs = new StreamWriter(@"C:\lab_works\Files\student.xml");
            p.Serialize(fs, a);
            fs.Close();
            XmlSerializer b = new XmlSerializer(typeof(Student));
            StreamReader sr = new StreamReader(@"C:\lab_works\Files\student.xml");
            Student s = new Student();
            s = (Student)b.Deserialize(sr);
            sr.Close();
            Console.WriteLine(s.name);
            Console.ReadKey();
        }
    }
}