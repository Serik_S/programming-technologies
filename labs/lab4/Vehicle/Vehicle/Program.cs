﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicle
{
    abstract class Vehicle
    {
        public int price;
        public string speed;
        public string Year;
        public int x, y;
        public void Set(int _x, int _y)
        {
            x = _x;
            y = _y;
        }
        public string Get()
        {
            return x + " " + y;
        }
        public override string ToString()
        {
            return price + " " + speed + " " + Year + "  ";
        }
    }
    class Plane : Vehicle
    {
        private int height;
        private int passangers;

        public Plane(int _height, int _passangers)
        {
            height = _height;
            passangers = _passangers;
        }
        public override string ToString()
        {
            return base.ToString() + " " + height + " " + passangers;
        }
    }
    class Car : Vehicle
    {
        private string color;
        private int probeg;
        private string mark;

        public Car(string _color, int _probeg, string _mark)
        {
            color = _color;
            probeg = _probeg;
            mark = _mark;
        }
        public override string ToString()
        {
            return base.ToString() + " " + color + " " + probeg + " " + mark;
        }
    }
    class Ship : Vehicle
    {
        private int passangers;
        private string port;

        public Ship(int _passangers, string _port)
        {
            passangers = _passangers;
            port = _port;
        }
        public override string ToString()
        {
            return base.ToString() + " " + passangers + " " + port;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Car a = new Car("black", 100000, "Audi");
            a.price = 50000;
            a.speed = "200 km/h";
            a.Year = "2012 year";
            Plane p = new Plane(10000, 35);
            p.price = 1000000;
            p.speed = "500 km/h";
            p.Year = "2013 year";
            Ship sh = new Ship(1000, "USA");
            sh.price = 350000;
            sh.speed = "100 km/h";
            sh.Year = "2010 year";
            Console.WriteLine(a);
            Console.WriteLine(p);
            Console.WriteLine(sh);
            Console.ReadKey();
        }
    }
}