﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstracts
{
    abstract class Figures
    {
        public double Area;
        public double Perimeter;
        
    }
    class Rectangle : Figures
    {
        private int x1, x2, x3, x4, y1, y2, y3, y4;
        public Rectangle(int _x1, int _y1, int _x2, int _y2, int _x3, int _y3, int _x4, int _y4)
        {
            x1 = _x1;
            y1 = _y1;
            x2 = _x2;
            y2 = _y2;
            x3 = _x3;
            y3 = _y3;
            x4 = _x4;
            y4 = _y4;
            this._Perimeter();
            this._area();
        }
        private void _area()
        {
            double a1, a2, a3;
            a1 = Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
            a2 = Math.Sqrt((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3));
            a3 = a1 * a2;
            this.Area = a3;
        }
        private void _Perimeter()
        {
            double p1, p2, p3;
            p1 = Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
            p2 = Math.Sqrt((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3));
            p3 = (p1 + p2) * 2;
            this.Perimeter = p3;
        }
    }
    class Circle : Figures
    {
        private int x1, x2;
        double R;
        public Circle(int _x1, int _x2, double _R)
        {
            x1 = _x1;
            x2 = _x2;
            R = _R;
            this._area();
            this._perimeter();
        }
        private void _area()
        {
            double a;
            a = 3.14 * R * R;
            this.Area = a;
        }
        private void _perimeter()
        {
            double p;
            p = 2 * 3.14 * R;
            this.Perimeter = p;
        }
    }
    class Triangle : Figures
    {
        int x1, x2, x3, y1, y2, y3;
        double p1, p2, p3, p;
        public Triangle(int _x1, int _y1, int _x2, int _y2, int _x3, int _y3)
        {
            x1 = _x1;
            y1 = _y1;
            x2 = _x2;
            y2 = _y2;
            x3 = _x3;
            y3 = _y3;
            this._perimeter();
            this._area();
        }
        private void _perimeter()
        {
            
            p1 = Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
            p2 = Math.Sqrt((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3));
            p3 = Math.Sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1));
            p = p1 + p2 + p3;
            this.Perimeter = p;
        }
        private void _area()
        {
            double a;
            double P = p/2;
            a = Math.Sqrt(P * (P - p1) * (P - p2) * (P - p3));
            this.Area = a;

        }
    }
    class Program
    {
        
        static void Main(string[] args)
        {
            Rectangle rec = new Rectangle(1, 1, 4, 4, 4, 10, 1, 10);
            Circle cir = new Circle(5, 5, 10);
            Triangle tri = new Triangle(1, 1, 5, 1, 3, 10);
            Console.WriteLine(rec.Area + " " + rec.Perimeter);
            Console.WriteLine(cir.Area + " " + cir.Perimeter);
            Console.WriteLine(tri.Area + " " + tri.Perimeter);
            Console.ReadKey();
        }
    }
}