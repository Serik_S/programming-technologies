﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace Streaming
{
    class Program
    {
        static void CompressFile(string inFilename, string outFilename)
        {
            FileStream sourceFile = File.Open(inFilename, FileMode.Open, FileAccess.Read);
            FileStream destFile = new FileStream(outFilename, FileMode.Create);
            GZipStream compStream = new GZipStream(destFile, CompressionMode.Compress);
            int theByte = sourceFile.ReadByte();
            while (theByte != -1)
            {
                compStream.WriteByte((byte)theByte);
                theByte = sourceFile.ReadByte();
            }
            compStream.Close();
            sourceFile.Close();
            destFile.Close();
            
        }
        static void UncompressFile(string inFilename, string outFilename)
        {
            FileStream sourceFile = File.Open(inFilename, FileMode.Open,FileAccess.Read);
            FileStream destFile = new FileStream(outFilename, FileMode.Create, FileAccess.Write);
            GZipStream compStream = new GZipStream(sourceFile, CompressionMode.Decompress);
            int theByte = compStream.ReadByte();
            while (theByte != -1)
            {
                destFile.WriteByte((byte)theByte);
                theByte = compStream.ReadByte();
            }
            compStream.Close();
            sourceFile.Close();
            destFile.Close();
            
        }
        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader(@"C:\lab_works\Files\Stream.txt");
            StreamWriter sw = new StreamWriter(@"C:\lab_works\Files\Stream2.txt");
            string[] numbers = sr.ReadToEnd().Split(' ');
            sr.Close();
            int p = int.Parse(numbers[0]);
            int[] newint = new int[p];
            
            int sum = 0;
            for (int i = 0; i < p; i++)
            {
                if (i == p - 1)
                {
                    newint[i] = int.Parse(numbers[numbers.Length-1]);
                    sum += newint[i];
                    sw.Write(newint[i] + " ");
                    break;
                } 
               newint[i] = int.Parse(numbers[i+1]);
               sum += newint[i];
               sw.Write(newint[i] + " ");
            }
            sw.Write(sum);
            sw.Close();
            CompressFile(@"C:\lab_works\Files\Stream2.txt", @"C:\lab_works\Files\Stream2.txt.gz");
            UncompressFile(@"C:\lab_works\Files\Stream2.txt.gz", @"C:\lab_works\Files\Stream3.txt");
            Console.ReadKey();
        }
    }
}