﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FSW
{
    class Program
    {
        static void Main()
        {
            // Write a string array to a file.
            string[] stringArray = new string[]
	{
	    "cat",
	    "dog",
	    "arrow"
	};
            File.WriteAllLines("file.txt", stringArray);
        }
    }

}
