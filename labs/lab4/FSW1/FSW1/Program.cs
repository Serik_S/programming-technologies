﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FSW1
{
    class Program
    {
        static void watcher_FileCreated(object sender, FileSystemEventArgs e)
        {
            //A new txt File has been created in C:/lab_works/Files
            Console.WriteLine("A new .txt file has been created");
        }
        static void Main(string[] args)
        {
            //create a new FileSystemWatcher
            FileSystemWatcher watcher = new FileSystemWatcher();
 
            //set the filter to only catch TXT files
            watcher.Filter = "*.txt";

            //Subscribe to the created event
            watcher.Created += new FileSystemEventHandler(watcher_FileCreated);
            //set the path
            watcher.Path = @"C:/lab_works/Files";
            //enable the FileSystemWatcher events
            Console.ReadKey();

        }
    }
}
