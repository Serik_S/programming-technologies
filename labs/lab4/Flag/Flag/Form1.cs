﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            Pen MyPen = new Pen(Color.Black, 2);
            Brush MyBrush = new SolidBrush(Color.Blue);
            Brush YBrush = new SolidBrush(Color.Yellow);
            g.DrawRectangle(MyPen, 300, 50, 300, 200);
            g.FillRectangle(MyBrush, 300, 50, 300, 200);
            g.FillRectangle(YBrush, 300, 120, 300, 30);
            g.FillRectangle(YBrush, 370, 50, 30, 200);
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

    }
}