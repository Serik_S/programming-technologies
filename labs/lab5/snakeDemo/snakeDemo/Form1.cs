﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace snakeDemo
{
    public partial class Form1 : Form
    {
        int x = 10, y = 10, dir = 0;
        Graphics g;

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            g = this.CreateGraphics();
            g.Clear(Color.White);
            Pen p = new Pen(Color.Black, 2);
            g.DrawEllipse(p, 10, 10, 5, 5);
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            if (dir == 0) x += 10;
            else if (dir == 1) y -= 10;
            else if (dir == 2) x -= 10;
            else if (dir == 3) y += 10;
            Draw();
        }
        private void Draw()
        {
            g.Clear(Color.White);
            g = this.CreateGraphics();
            Pen p = new Pen(Color.Black, 3);
            g.DrawEllipse(p, x, y, 3, 3);
        }

        private void onKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                dir = 0;
            }
            else if (e.KeyCode == Keys.Up)
            {
                dir = 1;
            }
            else if (e.KeyCode == Keys.Left)
            {
                dir = 2;
            }
            else if (e.KeyCode == Keys.Down)
            {
                dir = 3;
            }
        }
        
    }
}
