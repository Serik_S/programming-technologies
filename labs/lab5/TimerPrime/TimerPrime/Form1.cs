﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimerPrime
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int[] PN = new int[100];
        int k = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + PN[k++] + " ";
            if (k == 100) timer1.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PN[0] = 2;
            int n = 1;
            for (int i = 3; ; i++)
            {
                bool isPrime = true;
                for (int j = 2; j < i; j++)
                {
                    if (i % j == 0)
                    {
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime == true)
                {
                    PN[n] = i;
                    n++;
                }
                if (n == 100) break;
            }
            timer1.Enabled = true;
        }
    }
}
