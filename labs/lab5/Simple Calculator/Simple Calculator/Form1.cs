﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simple_Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int x, y;
            x = int.Parse(textBox1.Text);
            y = int.Parse(textBox2.Text);
            int z = x + y;
            textBox3.Text = z.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int x, y;
            x = int.Parse(textBox1.Text);
            y = int.Parse(textBox2.Text);
            int z = x - y;
            textBox3.Text = z.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int x, y;
            x = int.Parse(textBox1.Text);
            y = int.Parse(textBox2.Text);
            int z = x * y;
            textBox3.Text = z.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            double x, y;
            x = double.Parse(textBox1.Text);
            y = double.Parse(textBox2.Text);
            double z = x / y;
            textBox3.Text = z.ToString();
        }
    }
}
