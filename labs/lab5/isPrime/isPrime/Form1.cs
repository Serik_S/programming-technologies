﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace isPrime
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int n = int.Parse(textBox1.Text);


            for (int i = 2; i <= n - 1; i++)
            {
                if (n % i == 0)
                {
                    label1.Text = "Composite";
                    break;
                }
                label1.Text = "Primary";
            }
        }
    }
}
