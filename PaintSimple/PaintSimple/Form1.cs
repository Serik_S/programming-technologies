﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintSimple
{
    public partial class Form1 : Form
    {
        MyButton[,] mb = new MyButton[2,2];
        string[] st = new string[]{"P","L","R","E"};
        int act = 0;
        Button[,] colours = new Button[2,4];
        Color[] clrs = new Color[8] { Color.Black, Color.Violet, Color.Blue, Color.Red, Color.Orange, Color.Yellow, Color.Green, Color.White };
        Pen pen = new Pen(Color.Black);

        Bitmap bm;
        Graphics bGraph;
        Graphics sGraph;
        Paint pnt = new Paint();

        public Form1()
        {
            InitializeComponent();
            bm = new Bitmap(500, 250);
            bGraph = Graphics.FromImage(bm);
            sGraph = panel1.CreateGraphics();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int it1 = 0;
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    MyButton b = new MyButton(it1 + 1);
                    b.Text = st[it1];
                    b.Location = new Point(j * 25, 50 + i * 25);
                    b.Size = new Size(25, 25);
                    b.Click += Paint;
                    Controls.Add(b);
                    mb[i, j] = b;
                    it1++;
                }
            }
            int it2 = 0;
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Button b = new Button();
                    b.Text = "";
                    b.Location = new Point(60 + j*20, 290 + i*20);
                    b.Size = new Size(20, 20);
                    b.BackColor = clrs[it2];
                    b.Click += Colours;
                    Controls.Add(b);
                    colours[i, j] = b;
                    it2++;
                }
            }
            button1.BackColor = Color.Black; 
            sGraph.Clear(Color.White);
        }

        private void Paint(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            act = b.x;
            Console.WriteLine(act);
        }

        private void Colours(object sender, EventArgs e)
        {
            Button b = sender as Button;
            pen.Color = b.BackColor;
            button1.BackColor = b.BackColor;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            pen.Color = button1.BackColor;
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            pnt.a = e.Location;
            pnt.isPressed = true;
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            pnt.isPressed = false;
            int x = Math.Min(pnt.a.X, pnt.b.X);
            int y = Math.Min(pnt.a.Y, pnt.b.Y);
            int w = Math.Abs(pnt.a.X - pnt.b.X);
            int h = Math.Abs(pnt.a.Y - pnt.b.Y);

            if (act == 1)
            {
                
            }
            if (act == 2)
            {
                bGraph.DrawLine(pen, pnt.a, pnt.b);
            }
            if (act == 3)
            {
                bGraph.DrawRectangle(pen, x, y, w, h);
            }
            if (act == 4)
            {
                bGraph.DrawEllipse(pen, x, y, w, h);
            }
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            int x = Math.Min(pnt.a.X, pnt.b.X);
            int y = Math.Min(pnt.a.Y, pnt.b.Y);
            int w = Math.Abs(pnt.a.X - pnt.b.X);
            int h = Math.Abs(pnt.a.Y - pnt.b.Y);

            if (pnt.isPressed)
            {
                pnt.b = e.Location;
                if (act == 1)
                {
                    sGraph.DrawImage(bm, 0, 0);
                    sGraph.DrawLine(pen, pnt.a, pnt.b);
                    bGraph.DrawLine(pen, pnt.a, pnt.b);
                    pnt.a = pnt.b;
                }
                if (act == 2)
                {
                    sGraph.Clear(Color.White);
                    sGraph.DrawImage(bm, 0, 0);
                    sGraph.DrawLine(pen, pnt.a, pnt.b);
                }
                if (act == 3)
                {
                    sGraph.Clear(Color.White);
                    sGraph.DrawImage(bm, 0, 0);
                    sGraph.DrawRectangle(pen, x, y, w, h);
                }
                if (act == 4)
                {
                    sGraph.Clear(Color.White);
                    sGraph.DrawImage(bm, 0, 0);
                    sGraph.DrawEllipse(pen, x, y, w, h);
                }
            }
        }        
    }
}
