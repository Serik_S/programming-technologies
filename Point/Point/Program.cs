﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point
{
    public struct Point
    {
        public int x, y;
        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;
        }
        public override string ToString()
        {
            return x.ToString() + " "  + y.ToString();
        }
        public static Point operator + (Point a, Point b)
        {
            a.x += b.x;
            a.y += b.y;
            return a;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Point a = new Point(1, 2);
            Point b = new Point(3, 4);
            Point c = a + b;
            Console.WriteLine(c);
            Console.ReadKey();

        }
    }
}
