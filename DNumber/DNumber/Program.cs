﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fraction
{
    struct DNumber
    {
        int a, b;
        public DNumber(int _a, int _b)
        {
            a = _a;
            b = _b;
        }
        public override string ToString()
        {
            return a.ToString() + "/" + b.ToString();
        }
        public static int gcd(int a, int b)
        {
            if (a < b)
            {
                a = a + b;
                b = a - b;
                a = a - b;
            }
            if (b == 0) return a;
            return gcd(b, a % b);
        }
        public static DNumber operator *(DNumber arg1, DNumber arg2)
        {
            arg1.a *= arg2.a;
            arg1.b *= arg2.b;
            int t = DNumber.gcd(arg1.a, arg1.b);
            arg1.a /= t;
            arg1.b /= t;
            return arg1;
        }
        public static DNumber operator /(DNumber arg1, DNumber arg2)
        {
            arg1.a *= arg2.b;
            arg1.b *= arg2.a;
            int t = DNumber.gcd(arg1.a, arg1.b);
            arg1.a /= t;
            arg1.b /= t;
            return arg1;
        }
        public static DNumber operator +(DNumber arg1, DNumber arg2)
        {
            arg1.a = arg1.a * arg2.b + arg2.a * arg1.b;
            arg1.b *= arg2.b;
            int t = DNumber.gcd(arg1.a, arg1.b);
            arg1.a /= t;
            arg1.b /= t;
            return arg1;
        }
        public static DNumber operator -(DNumber arg1, DNumber arg2)
        {
            arg1.a = arg1.a * arg2.b - arg2.a * arg1.b;
            arg1.b *= arg2.b;
            int t = DNumber.gcd(Math.Abs(arg1.a), arg1.b);
            arg1.a /= t;
            arg1.b /= t;
            return arg1;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            DNumber a = new DNumber(1, 2);
            DNumber b = new DNumber(2, 3);
            Console.WriteLine(a * b);
            Console.WriteLine(a / b);
            Console.WriteLine(a + b);
            Console.WriteLine(a - b);
            Console.ReadKey();
        }
    }
}

