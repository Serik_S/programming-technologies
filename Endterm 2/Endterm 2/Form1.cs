﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Endterm_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        MyButton[,] mb = new MyButton[12, 12];

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    MyButton b = new MyButton(i, j);
                    b.Text = "";
                    b.Location = new Point(j * 25, i * 25);
                    b.Size = new Size(25, 25);
                    b.BackColor = Color.White;
                    b.Click += new EventHandler(btn_Click);
                    Controls.Add(b);
                    mb[i, j] = b;
                    if (i == 0 || j == 0 || i == 1 || j == 1 || i == 10 || j == 10 || i == 11 || j == 11)
                    {
                        b.Visible = false;
                    }
                }
            }
        }
        private void btn_Click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            if (mb[b.x, b.y].BackColor == Color.White) mb[b.x, b.y].BackColor = Color.Black;
            else mb[b.x, b.y].BackColor = Color.White;

            if (mb[b.x - 2, b.y - 1].BackColor == Color.White) mb[b.x - 2, b.y - 1].BackColor = Color.Black;
            else mb[b.x - 2, b.y - 1].BackColor = Color.White;
            if (mb[b.x - 1, b.y - 2].BackColor == Color.White) mb[b.x - 1, b.y - 2].BackColor = Color.Black;
            else mb[b.x - 1, b.y - 2].BackColor = Color.White;

            if (mb[b.x + 1, b.y - 2].BackColor == Color.White) mb[b.x + 1, b.y - 2].BackColor = Color.Black;
            else mb[b.x + 1, b.y - 2].BackColor = Color.White;
            if (mb[b.x + 2, b.y - 1].BackColor == Color.White) mb[b.x + 2, b.y - 1].BackColor = Color.Black;
            else mb[b.x + 2, b.y - 1].BackColor = Color.White;

            if (mb[b.x - 2, b.y + 1].BackColor == Color.White) mb[b.x - 2, b.y + 1].BackColor = Color.Black;
            else mb[b.x - 2, b.y + 1].BackColor = Color.White;
            if (mb[b.x - 1, b.y + 2].BackColor == Color.White) mb[b.x - 1, b.y + 2].BackColor = Color.Black;
            else mb[b.x - 1, b.y + 2].BackColor = Color.White;

            if (mb[b.x + 2, b.y + 1].BackColor == Color.White) mb[b.x + 2, b.y + 1].BackColor = Color.Black;
            else mb[b.x + 2, b.y + 1].BackColor = Color.White;
            if (mb[b.x + 1, b.y + 2].BackColor == Color.White) mb[b.x + 1, b.y + 2].BackColor = Color.Black;
            else mb[b.x + 1, b.y + 2].BackColor = Color.White;
        }
    }
}
