﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PractBN
{
    struct BigNumber
    {
        int[] a;
        int sz;
        public BigNumber(string s)
        {
            a = new int[1000];
            sz = s.Length;
            for (int i = sz - 1, j = 0; i >= 0; i--, j++)
            {
                a[j] = s[i] - '0';
            }
        }
        public static BigNumber operator+(BigNumber x, BigNumber y)
        {
            int k = 0;
            int sz;
            if (x.sz > y.sz) sz = x.sz;
            else sz = y.sz;

            for (int i = 0; i < sz; i++)
            {
                x.a[i] += y.a[i] + k;
                k = x.a[i] / 10;
                x.a[i] %= 10;
            }
            if (k > 0)
            {
                x.sz++;
                x.a[x.sz - 1] = k;
            }
            return x;
        }

        public override string ToString()
        {
            string s = "";
            for (int i = sz-1; i >= 0; i--)
            {
                s += a[i].ToString();
            }
            return s;
        }
        
       
        

    }
    class Program
    {
        static void Main(string[] args)
        {
            string s1 = Console.ReadLine();
            string s2 = Console.ReadLine();
            BigNumber x = new BigNumber(s1);
            BigNumber y = new BigNumber(s2);
            BigNumber z = new BigNumber();
            z = x + y;
            Console.WriteLine(z);
            Console.ReadKey();
        }
    }
}
