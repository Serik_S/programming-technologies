﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Midterm.items
{
    class Polygon
    {
        public List<Point> a;

        public Polygon(List<Point> list)
        {
            a = list;
        }

        public override string ToString()
        {
            string allps = string.Join(", ", a);
            return allps;
        }

        public double Perimeter ()
        {
            double per = 0; 
            for (int i = 0; i < a.Count; i++)
            {
                if (i == a.Count - 1)
                {
                    per += Line.lineLength(a[1],a[a.Count-1]);
                    break;
                }
                per += Line.lineLength(a[i],a[i+1]);
            }
            return per;
        }
        public static double operator *(Polygon a, Polygon b)
        {
            double sumper = a.Perimeter() + b.Perimeter();
            return sumper;
        }
    }
}
