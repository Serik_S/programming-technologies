﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(41, 41);
            Food food = new Food();
            Snake snake = new Snake();

            ConsoleKeyInfo keyInfo;

            while (true)
            {
                keyInfo = Console.ReadKey();
                switch (keyInfo.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (snake.Move(0, -1, food.location))
                        {
                            food = new Food();
                        }
                        break;
                    case ConsoleKey.DownArrow:
                        if (snake.Move(0, 1, food.location))
                        {
                            food = new Food();
                        }
                        break;
                    case ConsoleKey.LeftArrow:
                        if (snake.Move(-1, 0, food.location))
                        {
                            food = new Food();
                        }
                        break;
                    case ConsoleKey.RightArrow:
                        if (snake.Move(1, 0, food.location))
                        {
                            food = new Food();
                        }
                        break;
                    case ConsoleKey.Escape:
                        return;
                    default:
                        break;
                }
            }
        }
    }
}
