﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeConsole
{
    class Food
    {
        char sign = 'o';
        public Point location = new Point();

        public Food()
        {
            location.x = new Random().Next() % 40;
            location.y = new Random().Next() % 40;
            Show();
        }

        public void Show()
        {
            Console.SetCursorPosition(location.x, location.y);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(sign);
        }
    }
}
