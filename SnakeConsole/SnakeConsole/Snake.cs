﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeConsole
{
    class Snake
    {
        char sign = '*';
        List<Point> body = null;
        public Snake()
        {
            body = new List<Point>();
            body.Add(new Point {x = 10, y = 10});
            Show();
        }

        public void Show()
        {
            foreach (Point p in body)
            {
                Console.SetCursorPosition(p.x, p.y);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write(sign);
            }
        }

        public bool Move(int dx, int dy, Point floc)
        {
            Clear();
            if (floc.x == body[0].x + dx && floc.y == body[0].y + dy)
            {
                body.Insert(0, floc);
                Show();
                return true;
            }
            for (int i = body.Count - 1; i > 0; i--)
            {
                body[i].x = body[i - 1].x;
                body[i].y = body[i - 1].y;
            }
            body[0].x = body[0].x + dx;
            body[0].y = body[0].y + dy;
            Show();
            return false;
        }

        public void Clear()
        {
            foreach (Point p in body)
            {
                Console.SetCursorPosition(p.x, p.y);
                Console.ForegroundColor = ConsoleColor.Black;
                Console.Write(" ");
            }
        }
    }
}
