﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1.part1.ex2
{
    struct Person
    {
        public enum Genders : int { Male, Female };
        public string Firstname;
        public string Lastname;
        public int age;
        public Genders gender;
        public Person (string _Firstname, string _Lastname, int _age, Genders _gender)
        {
            Firstname = _Firstname;
            Lastname = _Lastname;
            age = _age;
            gender = _gender;
        }
        public override string ToString()
        {
            return Firstname + " " + Lastname + " (" + gender + "), age " + age;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person("Serik", "Seidigalimov", 18, Person.Genders.Male);
            Console.WriteLine(p.ToString());
            Console.ReadKey();
        }
    }
}
