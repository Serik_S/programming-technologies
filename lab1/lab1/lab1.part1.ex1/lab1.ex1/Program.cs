﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1.part1_ex1
{
    struct Person
    {
        public string Firstname;
        public string Lastname;
        public int age;
        public Person (string _Firstname, string _Lastname, int _age)
        {
            Firstname = _Firstname;
            Lastname = _Lastname;
            age = _age;
        }
        public override string ToString()
        {
            return Firstname + " " + Lastname + ", age " + age;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person("Serik", "Seidigalimov", 18);
            Console.WriteLine(p);
            Console.ReadKey();
                
        }
    }
}
