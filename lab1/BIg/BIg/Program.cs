﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIg
{
    struct BigNumber
    {
        public int[] a;
        public int sz;
        public BigNumber(string s)
        {
            sz = s.Length;
            a = new int[1000];
            for (int i =s.Length-1, j = 0; i>=0; i--, j++){
                a[j] = s[i] - '0';
            }
        }
        public override string ToString()
        {
            string s = "";
            for (int i = sz-1; i >=0; i--)
            {
                s += a[i].ToString();

            }
            return s;
        }
        
        public static BigNumber operator +(BigNumber x, BigNumber y)
        {
            BigNumber d = new BigNumber ("0");
            int k = 0;
            int len = Math.Max(x.sz, y.sz);
            d.sz = len;
            for (int i = 0; i<len; i++)
            {
                d.a[i] = x.a[i] + y.a[i] + k;
                k = d.a[i] / 10;
                d.a[i] %= 10;
            }
            if (k > 0)
            {
                d.sz++;
                d.a[d.sz - 1] = k;
            }
            
           
            return d;
        }
        
            

    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the first number:");
            string firstNumber = Console.ReadLine();
            Console.WriteLine("Enter the second number:");
            string secondNumber = Console.ReadLine();
            BigNumber a = new BigNumber(firstNumber);
            BigNumber b = new BigNumber(secondNumber);
            BigNumber d = a + b;
            Console.WriteLine(d);
            Console.ReadKey();
        }
    }
}
