﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIg
{
    struct BigNumber
    {
        public int[] a;
        public int sz;
        public BigNumber(string s)
        {
            sz = s.Length;
            a = new int[1000];
            for (int i = s.Length - 1, j = 0; i >= 0; i--, j++)    //we add the bignumber into an array in reverse order
            {
                a[j] = s[i] - '0';
            }
        }
        public override string ToString()
        {
            string s = "";
            int init = sz - 1;
            while (a[init] == 0 && init != 0) init--;  //here we delete 0, 
            for (int i = init; i >= 0; i--)  //we print from the numbers which are not 0
            {
                if (a[i] == 45) s += "-"; //in ascii 45 is the sign of minus
                else s += a[i].ToString(); //we add the numbers into a string

            }
            return s;   //here we show the answer
        }
        static bool bisbigger(BigNumber x, BigNumber y)
        {
            if (x.sz > y.sz) return false;
            if (x.sz < y.sz) return true;
            for (int i = x.sz - 1; i >= 0; i--)
            {
                if (x.a[i] < y.a[i]) return true;
            }
            return false;
        }
        public static void Swap<T>(ref T left, ref T right)
        {
            T temp = left;
            left = right;
            right = temp;
        }
        public static BigNumber operator -(BigNumber x, BigNumber y)
        {
            bool swapped = false;   //initially swapped will be false
            if (bisbigger(x, y))   //here we check is the second number is bigger than the first
            {
                Swap<BigNumber>(ref x, ref y);
                swapped = true;   //if the it is bigger then swapped will be true
            }
            BigNumber d = new BigNumber("0");
            int len = Math.Max(x.sz, y.sz);   //here we check which length is bigger
            d.sz = len;
            for (int i = 0; i < len; i++)   //here we make calculation
            {
                if (x.a[i] < y.a[i])
                {
                    d.a[i] = x.a[i] + 10 - y.a[i];
                    x.a[i + 1] -= 1;
                }
                else
                {
                    d.a[i] = x.a[i] - y.a[i];
                }
            }
            if (swapped == true)
            {
                d.a[d.sz] = (int)'-';    //if the second number will be bigger than the first, then its sign will have minus
                d.sz++;                  //as we added new sign then the size of d will increase by 1
            }
            return d;
        }



    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the first number:");
            string firstNumber = Console.ReadLine();
            Console.WriteLine("Enter the second number:");
            string secondNumber = Console.ReadLine();
            BigNumber a = new BigNumber(firstNumber);
            BigNumber b = new BigNumber(secondNumber);
            BigNumber d = a - b;
            Console.WriteLine(d);
            Console.ReadKey();
        }
    }
}
