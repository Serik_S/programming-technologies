﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    public struct DNumber
    {
        public int a, b;
        public DNumber(int _a, int _b)
        {
            a = _a;
            b = _b;
        }


        public override string ToString()
        {
            return a.ToString() + "/" + b.ToString();
        }
        public void Normalise()
        {
            int x = Math.Abs(a);
            int y = Math.Abs(b);
            while (x > 0 && y > 0)
            {
                if (x > y)
                    x = x % y;
                else
                    y = y % x;
            }
            x = x + y;
            a /= x;
            b /= x;
        }

        public static DNumber operator -(DNumber arg1, DNumber arg2)
        {
            arg1.a *= arg2.b;
            arg2.a *= arg1.b;
            arg1.a -= arg2.a;
            arg1.b *= arg2.b;
            arg1.Normalise();
            return arg1;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            DNumber a = new DNumber(5, 2);
            DNumber b = new DNumber(10, 3);
            DNumber c = a - b;
            Console.WriteLine(c);
            Console.ReadKey();
        }
    }
}
