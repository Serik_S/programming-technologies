﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SnakeGame
{
    class Wall
    {
        public List<Point> obst;
        public Wall(int n)
        {
            obst = new List<Point>();
            for (int i = 0; i < n; i++)
            {
                Point p = new Point { X = new Random().Next() % 501, Y = new Random().Next() % 501 };
                obst.Add(p);
            }
        }
    }
}
