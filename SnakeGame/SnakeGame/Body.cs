﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SnakeGame
{
    class Body
    {
        public List<Point> body = null;
        public Body()
        {
            body = new List<Point>();
            body.Add(new Point { X = 50, Y = 25 });
        }

        public bool Move(int dx, int dy, Point floc)
        {
            if (floc.X == body[0].X + dx && floc.Y == body[0].Y + dy)
            {
                body.Insert(0, floc);
                return true;
            }
            for (int i = body.Count - 1; i > 0; i--)
            {
                Point p = body[i-1];
                body.Remove(body[i]);
                body.Insert(i, p);
            }
            Point pp = new Point { X = body[0].X + dx, Y = body[0].Y + dy };
            body.Remove(body[0]);
            body.Insert(0, pp);
            return false;
        }
    }
}
