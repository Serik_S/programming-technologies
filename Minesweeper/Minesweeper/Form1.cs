﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Minesweeper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int n = 10;
        int m = 10;
        int k = 10;
        Saper s;
        int time = 0;
        MyButton[,] mb;

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Text = k.ToString();
            s = new Saper(n, m, k);
            mb = new MyButton[n + 2, m + 2];
            for (int i = 0; i < n + 2; i++)
            {
                for (int j = 0; j < m + 2; j++)
                {
                    MyButton b = new MyButton(i,j);
                    b.Text = "";
                    b.Location = new Point (j*25,i*25);
                    b.Size = new Size(25,25);
                    b.Click += new EventHandler(btn_Click);
                    Controls.Add(b);
                    mb[i, j] = b;
                    if (i == 0 || j == 0 || i == n + 1 || j == m + 1)
                    {
                        b.Visible = false;
                    }
                }
            }
        }
        private void btn_Click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            int a = s.Move(b.x, b.y);
            if (a == -1) 
            { 
                b.Text = "*";
                s.GameOver();
                var result = MessageBox.Show("Game Over");

            }
            else b.Text = a.ToString();
            nulls(b.x, b.y);
        }

        private void nulls(int x, int y)
        {
            mb[x, y].Enabled = false;
            if (mb[x, y].Text != "0") return;
            nulls(x + 1, y + 1);
            nulls(x, y + 1);
            nulls(x - 1, y + 1);
            nulls(x + 1, y);
            nulls(x - 1, y);
            nulls(x + 1, y - 1);
            nulls(x, y - 1);
            nulls(x - 1, y - 1);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            time++;
            int M = time / 60;

            int m1 = M / 10;
            int m2 = M % 10;

            int S = time % 60;

            int s1 = S / 10;
            int s2 = S % 10;

            label2.Text = m1.ToString() + m2 + ":" + s1 + s2;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
