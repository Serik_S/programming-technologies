﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace First100Primes
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int[] primes = new int[100];
        int b = 0;
        private void textBox1_TextChanged(object sender, EventArgs e)
        {            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            primes [0] = 2;  
            int a = 1;
            for (int i = 3; ; i++)
            {
                bool check = true;
                for (int j = 2; j < i; j++)
                {
                    if (i % j == 0)
                    {
                        check = false;
                        break;
                    }
                }
                if (check)
                {
                    primes[a] = i;
                    a++;
                }
                if (a == 100) break;
            }
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + primes[b++] + " ";
            if (b == 100) timer1.Enabled = false;
        }
    }
}
