﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab1Part2Ex3
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TextReader tr = new StreamReader(locationTextBox.Text);
                try
                { displayTextBox.Text = tr.ReadToEnd(); }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
                finally
                { tr.Close(); }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

        }
    }
}
