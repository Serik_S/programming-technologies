﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1Part1
{
    struct Person
    {
        public enum Genders : int { male, female };

        public string FirstName;
        public string LastName;
        public int age;
        public Genders gender;

        public Person (string _FirstName, string _LastName, int _age, Genders _gender) {
            FirstName = _FirstName;
            LastName = _LastName;
            age = _age;
            gender = _gender;
        }
        public override string ToString() {
            return FirstName + " " + LastName + " (" + gender + "), age " + age;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person("Tony", "Allen", 32, Person.Genders.male);
            Console.WriteLine(p);
            Console.ReadKey();
        }
    }
}
