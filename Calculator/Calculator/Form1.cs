﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Calc calc = new Calc();
        MyButton[] mb = new MyButton[10];
        MyButton[] op = new MyButton[4];
        string[] sign = new string[4] { "+", "-", "*", "/" };

        int oper = 0;
        bool check = false;

        Bignumber res = new Bignumber("0");
        Bignumber bmem = new Bignumber("0");

        private void Form1_Load(object sender, EventArgs e)                     //Creation of buttons
        {
            int n = 0;
            for (int i = 0; i < 1; i++)
            {
                mb[0] = new MyButton(n);
                mb[0].Location = new Point(190, 390);
                mb[0].Size = new Size(60, 60);
                mb[0].Text = n.ToString();
                mb[0].Click += num_Click;
                Controls.Add(mb[0]);
                n++;
            }
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    mb[n] = new MyButton(n);
                    mb[n].Location = new Point(130 + j * 60, 330 - i * 60);
                    mb[n].Size = new Size(60, 60);
                    mb[n].Text = n.ToString();
                    mb[n].Click += num_Click;
                    Controls.Add(mb[n]);
                    n++;
                }
            }
            for (int i = 0; i < 4; i++)
            {
                op[i] = new MyButton(i + 1);
                op[i].Location = new Point(310, 390 - i * 60);
                op[i].Size = new Size(60, 60);
                op[i].Text = sign[i];
                op[i].Click += operation;
                Controls.Add(op[i]);
            }
        }

        private void num_Click(object sender, EventArgs e)                      //Typing of numbers
        {
            MyButton b = sender as MyButton;
            textBox1.Text += b.Text;
        }

        private void operation(object sender, EventArgs e)                      //Operations with numbers
        {
            MyButton b = sender as MyButton;
            oper = b.x;
            if (check == false)
            {
                calc.a = new Bignumber(textBox1.Text);
                textBox1.Text = "";
                check = true;
            }
        }

        private void equals_Click(object sender, EventArgs e)                   //Result
        {
            if (check)
            {
                calc.b = new Bignumber(textBox1.Text);
                res = calc.Count(calc.a, calc.b, oper);
                textBox1.Text = res.s;
                check = false;
            }
            /*if (check == false)
            {
                calc.a = res;
                res = calc.Count(calc.a, calc.b, oper);
                textBox1.Text = res.s;
            }*/
        }

        private void Mplus_Click(object sender, EventArgs e)                    //Memory
        {
            bmem = bmem + new Bignumber(textBox1.Text);
        }

        private void Mminus_Click(object sender, EventArgs e)
        {
            bmem = bmem - new Bignumber(textBox1.Text);
        }

        private void mRead_Click(object sender, EventArgs e)                    
        {
            textBox1.Text = bmem.s;
        }

        private void MClear_Click(object sender, EventArgs e)
        {
            bmem = new Bignumber("0");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Clear_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            calc = new Calc();
            check = false;
            oper = 0;
            res = new Bignumber("0");
        }
    }
}
