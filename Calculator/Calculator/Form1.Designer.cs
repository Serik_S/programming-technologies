﻿namespace Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.equals = new System.Windows.Forms.Button();
            this.mRead = new System.Windows.Forms.Button();
            this.Mplus = new System.Windows.Forms.Button();
            this.Mminus = new System.Windows.Forms.Button();
            this.MClear = new System.Windows.Forms.Button();
            this.Clear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(130, 120);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(240, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // equals
            // 
            this.equals.Location = new System.Drawing.Point(250, 390);
            this.equals.Name = "equals";
            this.equals.Size = new System.Drawing.Size(60, 60);
            this.equals.TabIndex = 1;
            this.equals.Text = "=";
            this.equals.UseVisualStyleBackColor = true;
            this.equals.Click += new System.EventHandler(this.equals_Click);
            // 
            // mRead
            // 
            this.mRead.Location = new System.Drawing.Point(250, 150);
            this.mRead.Name = "mRead";
            this.mRead.Size = new System.Drawing.Size(60, 60);
            this.mRead.TabIndex = 2;
            this.mRead.Text = "MR";
            this.mRead.UseVisualStyleBackColor = true;
            this.mRead.Click += new System.EventHandler(this.mRead_Click);
            // 
            // Mplus
            // 
            this.Mplus.Location = new System.Drawing.Point(130, 150);
            this.Mplus.Name = "Mplus";
            this.Mplus.Size = new System.Drawing.Size(60, 60);
            this.Mplus.TabIndex = 3;
            this.Mplus.Text = "M+";
            this.Mplus.UseVisualStyleBackColor = true;
            this.Mplus.Click += new System.EventHandler(this.Mplus_Click);
            // 
            // Mminus
            // 
            this.Mminus.Location = new System.Drawing.Point(190, 150);
            this.Mminus.Name = "Mminus";
            this.Mminus.Size = new System.Drawing.Size(60, 60);
            this.Mminus.TabIndex = 4;
            this.Mminus.Text = "M-";
            this.Mminus.UseVisualStyleBackColor = true;
            this.Mminus.Click += new System.EventHandler(this.Mminus_Click);
            // 
            // MClear
            // 
            this.MClear.Location = new System.Drawing.Point(310, 150);
            this.MClear.Name = "MClear";
            this.MClear.Size = new System.Drawing.Size(60, 60);
            this.MClear.TabIndex = 5;
            this.MClear.Text = "MC";
            this.MClear.UseVisualStyleBackColor = true;
            this.MClear.Click += new System.EventHandler(this.MClear_Click);
            // 
            // Clear
            // 
            this.Clear.Location = new System.Drawing.Point(130, 390);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(60, 60);
            this.Clear.TabIndex = 6;
            this.Clear.Text = "C";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.MClear);
            this.Controls.Add(this.Mminus);
            this.Controls.Add(this.Mplus);
            this.Controls.Add(this.mRead);
            this.Controls.Add(this.equals);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button equals;
        private System.Windows.Forms.Button mRead;
        private System.Windows.Forms.Button Mplus;
        private System.Windows.Forms.Button Mminus;
        private System.Windows.Forms.Button MClear;
        private System.Windows.Forms.Button Clear;


    }
}

