﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckPrime
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public string CheckPrime(int a)
        {
            if (a < 1) return "write a proper integer";
            if (a == 1) return "true";

            bool isPrime = true;

            for (int i = 2; i < a; i++)
            {
                if (a % i == 0)
                {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime) return "true";
            else return "false";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int a = int.Parse(label1.Text);
            textBox1.Text = CheckPrime(a);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
