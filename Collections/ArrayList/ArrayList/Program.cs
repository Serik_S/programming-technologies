﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myArrayList
{

public class SamplesArrayList  {

   public static void Main()  {
      ArrayList myAL = new ArrayList();
      myAL.Add("Hello");
      myAL.Add("World");
      myAL.Add("!");

      Console.WriteLine( "myAL" );
      Console.WriteLine( "Count:    {0}", myAL.Count );
      Console.WriteLine( "Capacity: {0}", myAL.Capacity );
      Console.ReadKey();
   }

  
}

}
 
