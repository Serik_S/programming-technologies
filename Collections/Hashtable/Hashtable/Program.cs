﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleWithHashtable
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable students = new Hashtable();

            students.Add(15, "Sanzhar");
            students.Add(14, "Serik");
            students.Add(16, "Tamerlan");
            Console.ReadKey();
        }
    }
}
