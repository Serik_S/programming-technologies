﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue myQ = new Queue();

            myQ.Enqueue("Serik");
            myQ.Enqueue("Sanzhar");
            myQ.Enqueue("Tamerlan");
            
            Console.WriteLine("There are {0} students who want to have full on programming", myQ.Count);
            Console.ReadKey();  
        }
    }
}
