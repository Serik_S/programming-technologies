﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsingStack
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack Numbers = new Stack();

            Numbers.Push("First");
            Numbers.Push("Second");
            Numbers.Push("Third");
            Numbers.Push("Fourth");

            while (Numbers.Count > 0)
            {
                Console.WriteLine(Numbers.Pop());
            }
            Console.ReadKey();
        }
    }
}
