﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Crest
{
    public partial class Form1 : Form
    {
        MyButton[,] bt  = new MyButton[10,10];
        public Form1()
        {
            InitializeComponent();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    bt[i, j] = new MyButton(i, j);
                    bt[i, j].Click += new EventHandler(btn_click);
                    bt[i, j].BackColor = Color.White;
                    bt[i, j].Size = new Size(30, 30);
                    bt[i, j].Location = new Point(i * 30, j * 30);
                    this.Controls.Add(bt[i, j]);
                }
            }
        }
        private void btn_click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            for (int i = 0; i < 10; i++)
            {
                if (bt[b.x, i].BackColor == Color.Red) bt[b.x, i].BackColor = Color.White;
                else bt[b.x, i].BackColor = Color.Red;
                if (bt[i, b.y].BackColor == Color.Red) bt[i, b.y].BackColor = Color.White;
                else bt[i, b.y].BackColor = Color.Red;
            }
            if (bt[b.x, b.y].BackColor == Color.Red) 
                { 
                    bt[b.x, b.y].BackColor = Color.White; 
                }
            else bt[b.x, b.y].BackColor = Color.Red;
        }
    }
}
