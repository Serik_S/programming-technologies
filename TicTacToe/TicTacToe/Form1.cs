﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Form1 : Form
    {
        Field f = new Field();
        MyButton[,] mb;
        bool isX = true;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            mb = new MyButton[3, 3];
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    MyButton b = new MyButton(i, j);
                    b.Text = "";
                    b.Location = new Point(100 + j * 100, 100 + i * 100);
                    b.Size = new Size(100, 100);
                    b.Click += button_Click;
                    Controls.Add(b);
                    mb[i, j] = b;
                }
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            if (isX)
            {
                b.Text = "X";
                f.f[b.x, b.y] = 1;
                isX = false;
            }
            else
            {
                b.Text = "O";
                f.f[b.x, b.y] = 2;
                isX = true;
            }
            b.Enabled = false;

            if (f.Check() != 0 && f.Check() != 4)
            {
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (f.f[i, j] == 3)
                        {
                            mb[i, j].BackColor = Color.Red;
                        }
                    }
                }
                if (b.Text == "X")
                    MessageBox.Show("Player 1 has won");
                else MessageBox.Show("Player 2 has won");
                //MessageBox.Show("Player " + f.Check().ToString() + " has won");

                Restart();
            }

            if (f.Check() == 4)
            {
                MessageBox.Show("Draw");
                Restart();
            }
        }
        private void Restart()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Controls.Remove(mb[i, j]);
                }
            }

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    MyButton b = new MyButton(i, j);
                    b.Text = "";
                    b.Location = new Point(100 + j * 100, 100 + i * 100);
                    b.Size = new Size(100, 100);
                    b.Click += button_Click;
                    Controls.Add(b);
                    mb[i, j] = b;
                }
            }
            isX = true;
            f = new Field();
        }
    }
}
