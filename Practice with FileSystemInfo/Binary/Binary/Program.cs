﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Binary
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream file = File.Create("c:/lab_works/file.txt");
            BinaryWriter writer = new BinaryWriter(file);

            long number = 100;
            byte[] bytes = { 10, 20, 50, 100 };
            string s = "hello world";

            writer.Write(number);
            writer.Write(bytes);
            writer.Write(s);
            writer.Close();
           // FileStream stream = File.Open("c:/lab_works/file.txt");
            Console.ReadKey();
        }
    }
}
