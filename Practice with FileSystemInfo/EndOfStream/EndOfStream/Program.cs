﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EndOfStream
{
    class Program
    {
        static void Main(string[] args)
        {
            String path = @"c:/lab_works/file.txt";
            StreamReader sr = new StreamReader(path);
            while (!sr.EndOfStream)
            {
                Console.WriteLine(sr.ReadLine());
            }
            sr.Close();
            Console.ReadKey();

        }
    }
}
