﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace String_reader
{
    class Program
    {
        static void Main(string[] args)
        {
            String s = @"this is 
            multiline 
            text string";

            StringReader sr = new StringReader(s);
            while (sr.Peek() != -1)
            {
                string line = sr.ReadLine();
                Console.WriteLine(line);
            }
            Console.ReadKey();
        }
    }
}
