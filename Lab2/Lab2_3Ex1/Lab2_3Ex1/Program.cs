﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace CompressionDEMO
{
    class Program
    {
        static void CompressFile(string inFilename, string outFilename)
        {
            FileStream sourceFile = File.Open(inFilename, FileMode.Open, FileAccess.Read);
            FileStream destFile = new FileStream(outFilename, FileMode.Create);
            GZipStream compStream = new GZipStream(destFile, CompressionMode.Compress);
            int theByte = sourceFile.ReadByte();
            while (theByte != -1)
            {
                compStream.WriteByte((byte)theByte);
                theByte = sourceFile.ReadByte();
            }
        }
        static void UncompressFile(string inFilename, string outFilename)
        {
            FileStream sourceFile = File.Open(inFilename, FileMode.Open, FileAccess.Read);
            FileStream destFile = new FileStream(outFilename, FileMode.Create);
            GZipStream compStream = new GZipStream(destFile, CompressionMode.Decompress);
            int theByte = compStream.ReadByte();
            while (theByte != -1)
            {
                destFile.WriteByte((byte)theByte);
                theByte = compStream.ReadByte();
            }
        }
        static void Main(string[] args)
        {
            CompressFile(@"c:\Аслан\boot.ini", @"c:\Аслан\boot.ini.gz");
            UncompressFile(@"c:\Аслан\boot.ini.gz", @"c:\boot.ini.test");
        }
    }
}
