﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Snake
{
    class Body
    {
        public Point[] body;
        public int length;
        public int direction;

        public Body()
        {
            body = new Point[21];
            NewGame();
        }

        public void NewGame()
        {
            body[0] = new Point(5, 5);
            length = 1;
            direction = 1;
        }

        public void Move()
        {
            for (int i = length - 1; i > 0; i--)
            {
                body[i] = body[i - 1];
            }
        }

        public void GoRight()
        {
            Move();
            direction = 1;
            body[0].X ++;
            if (body[0].X > 30) body[0].X -= 30;
        }
        public void GoDown()
        {
            Move();
            direction = 2;
            body[0].Y++;
            if (body[0].Y > 30) body[0].Y -= 30;
        }

        public void GoLeft()
        {
            Move();
            direction = 3;
            body[0].X--;
            if (body[0].X < 0) body[0].X += 30;
        }

        public void GoUp()
        {
            Move();
            direction = 4;
            body[0].Y--;
            if (body[0].Y < 0) body[0].Y += 30;
        }

        public void IncreaseLength()
        {
            length++;
        }
    }
}
