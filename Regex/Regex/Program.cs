﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace regex
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = Console.ReadLine();
            if (Regex.IsMatch(input, @"^([A-Z]|[a-z]|[0-9]){1}.{2,}@[a-z]*\.[a-z]*$"))
            {
                Console.WriteLine("accepted");
            }
            else
            {
                Console.WriteLine("denied");
            }
            Console.ReadKey();
        }
    }
}