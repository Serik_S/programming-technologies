﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _9buttons
{
    public partial class Form1 : Form
    {
        string r = "";
        public Form1()
        {
            InitializeComponent();
            int y = 20, p = 2;
            string s = "1";
            
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    Button bt = new Button();
                    bt.Click += new System.EventHandler(this.button1_Click);
                    bt.Text = s;
                    bt.Size = new Size(40, 25);
                    y += 30;
                    s = p.ToString();
                    p++;
                    bt.Location = new Point(i * 100, j * 100);
                    this.Controls.Add(bt);
                }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Button bt = sender as Button;
            r += bt.Text;
            textBox1.Text = r;
        }
    }
}
