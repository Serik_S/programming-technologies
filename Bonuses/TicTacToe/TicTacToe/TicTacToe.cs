﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class TicTacToe
    {        
        int[,] ar = new int[3,3];
        int p = 0;    
        public TicTacToe()
        {

        }
        public void move(int x, int y)
        {
            if (ar[x, y] != 0)
                return;
            if (p == 0) ar[x, y] = 1;
            else ar[x, y] = 2;
            p = (p + 1) % 2;   
        }
        public int check()
        {
            for (int i = 0; i < 3;  i++)
            {
                if (ar[0, i] == ar[1, i] && ar[1, i] == ar[2, i] && ar[0, i] == 1) return 1;
                if (ar[i, 0] == ar[i, 1] && ar[i, 1] == ar[i, 2] && ar[i, 0] == 1) return 1;
                if (ar[0, 0] == ar[1, 1] && ar[1, 1] == ar[2, 2] && ar[1, 1] == 1) return 1;
                if (ar[0, 2] == ar[1, 1] && ar[1, 1] == ar[2, 0] && ar[0, 2] == 1) return 1;
            }
            for (int i = 0; i < 3; i++)
            {
                if (ar[0, i] == ar[1, i] && ar[1, i] == ar[2, i] && ar[0, i] == 2) return 2;
                if (ar[i, 0] == ar[i, 1] && ar[i, 1] == ar[i, 2] && ar[i, 0] == 2) return 2;
                if (ar[0, 0] == ar[1, 1] && ar[1, 1] == ar[2, 2] && ar[1, 1] == 2) return 2;
                if (ar[0, 2] == ar[1, 1] && ar[1, 1] == ar[2, 0] && ar[0, 2] == 2) return 2;
            }
            for (int i = 0; i < 3; i++)
            {
                if (ar[0, i] != ar[1, i] && ar[0, i] != ar[2, i]) return 3;
                if (ar[i, 0] != ar[i, 1] && ar[i, 0] != ar[i, 2]) return 3;
                if (ar[0, 0] != ar[1, 1] && ar[0, 0] != ar[2, 2]) return 3;
                if (ar[0, 2] != ar[1, 1] && ar[0, 2] != ar[2, 0]) return 3;
            }
                return 0;
        }
    }
}
