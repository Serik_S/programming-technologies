﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Form1 : Form
    {
        int q = 1;
        TicTacToe tte = new TicTacToe();
        MyButton[,] bt = new MyButton[3,3];
        public Form1()
        {
            InitializeComponent();
            string s = "";
            for (int i = 0; i < 3; i++) 
            {
                for (int j = 0; j <3; j++)
                {
                    bt[i, j] = new MyButton(i, j);
                    //tte.move(i,j);
                    bt[i,j].Click += new System.EventHandler(this.button1_Click);
                    bt[i,j].Text = s;
                    bt[i, j].BackColor = Color.White;
                    bt[i,j].Size = new Size(100,100);
                    bt[i,j].Location = new Point(i * 100, j * 100);
                    this.Controls.Add(bt[i,j]);
                }
            }
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            int counter = 0;
            string message = "Do you want to play again?";
            MyButton btns = sender as MyButton;
            tte.move(btns.x, btns.y);
            if (q % 2 == 0) btns.Text = "0";
            else btns.Text = "X";
            q++;
            btns.Enabled = false;
            if (tte.check() == 1) label1.Text = "Winner is Player #1";
            else if (tte.check() == 2)
            { 
                label1.Text = "Winner is Player #2";
            }
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    if (bt[i,j].Enabled == false)
                    {
                        counter++;
                    }
                }
            if (counter == 9)
            {
                label1.Text = "Draw";
            }
                    if (tte.check() == 1)
                    {
                        DialogResult result1 = MessageBox.Show(message, label1.Text, MessageBoxButtons.YesNo);
                        if (result1 == DialogResult.Yes)
                        {
                            restart();
                            q = 1;
                        }
                        else Close();
                    }
                    else if (tte.check() == 2)
                    {
                        DialogResult result1 = MessageBox.Show(message, label1.Text, MessageBoxButtons.YesNo);
                        if (result1 == DialogResult.Yes)
                        {
                            q = 1;
                            restart();
                        }
                        else Close();
                    }
        }
        public void restart()
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    bt[i, j].Text = "";
                    bt[i, j].BackColor = Color.White;
                    bt[i, j].Enabled = true;
                }
            label1.Text = "";
            tte = new TicTacToe();
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
