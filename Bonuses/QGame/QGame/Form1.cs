﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace QGame
{
    public partial class Form1 : Form
    {
        Graphics g;
        int x = 10, y = 10, dx = 5, dy = 5;
        int n = 285;
        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            if (x + dx > 830 || x + dx < 0)
            {
                dx = -dx;
            }
            if ((y + dy >= 268  && (x >= n && x <= n + 100)) || y + dy < 0)
            {
                dy = -dy;
            }
            
            x = x + dx;
            y = y + dy;
            Draw();

            DoubleBuffered = true;
            if (y > this.Height - 50)
            {
                timer1.Enabled = false;
                const string message = "You Lose! Do you want to start again?";
                DialogResult result = MessageBox.Show(message, "important question", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes) restart();
                else Close();
            }
        }
        private void Draw()
        {
            DoubleBuffered = true;
            g.Clear(Color.White);
            g = this.CreateGraphics();
            Pen p = new Pen(Color.Blue, 5);
            Brush MyBrush = new SolidBrush(Color.Black);
            g.DrawEllipse(p, x, y, 10, 10);
            g.DrawRectangle(p, 100, 100, 10, 10);
            g.DrawRectangle(p, 700, 50, 10, 10);
            g.FillRectangle(MyBrush, n, 268, 100, 20);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            DoubleBuffered = true;
            g = this.CreateGraphics();
            g.Clear(Color.White);
            Pen p = new Pen(Color.Blue, 5);
            Brush MyBrush = new SolidBrush(Color.Black);
            g.DrawEllipse(p, x, y, 10, 10);
            g.DrawRectangle(p, 100, 100, 10, 10);
            g.DrawRectangle(p, 700, 50, 10, 10);
            g.FillRectangle(MyBrush, n, 268, 100, 20);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            DoubleBuffered = true;
            Brush MyBrush = new SolidBrush(Color.Black);
            if (e.KeyCode == Keys.Right)
            {
                g.FillRectangle(MyBrush, n+=50, 268, 100, 20);
            }
            if (e.KeyCode == Keys.Left)
            {
                g.FillRectangle(MyBrush, n -= 50, 268, 100, 20);
            }            
        }
        private void restart()
        {
            x = 10;
            y = 10;
            n = 260;
            timer1.Enabled = true;
        }
    }
}
