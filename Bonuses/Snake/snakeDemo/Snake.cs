﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using System.Drawing;

namespace snakeDemo
{
    public class Snake
    {
        public int x, y, direction, foodx, foody, width, height, eaten;
        public Wall wall;
        public ArrayList a;
        public Snake(int _width, int _height, Wall _wall)
        {
            a = new ArrayList();
            x = 0;
            y = 0;
            wall = _wall;
            eaten = 0;
            width = _width;
            height = _height;
            a.Add(new Point(1, 1));
            direction = 3;
        }

        public int move(int d)
        {
            switch (d)
            {
                case 0:
                    direction = 0;
                    x = x - 5;
                    break;
                case 1:
                    direction = 1;
                    x = x + 5;
                    break;
                case 2:
                    direction = 2;
                    y = y - 5;
                    break;
                case 3:
                    direction = 3;
                    y = y + 5;
                    break;
            }
            foreach (Point p in a)
            {
                if (x == p.X && y == p.Y)
                {
                    return -1;
                }
            }
            foreach (Point p in wall.wall)
            {
                if (x == p.X && y == p.Y)
                {
                    return -1;
                }
            }
            if (x < 0 || y < 0 || x > width || y > height)
            {
                return -1;
            }
            if (eaten == 0)
            {
                a.RemoveAt(a.Count - 1);
            }
            else
            {
                eaten = 0;
            }
            a.Insert(0, new Point(x, y));
            return 0;
        }
        public int eat()
        {
            if (x == foodx && y == foody)
            {
                return 1;
            }
            return 0;
        }
            
    }
}
