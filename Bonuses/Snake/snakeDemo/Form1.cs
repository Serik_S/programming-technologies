﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace snakeDemo
{
    public partial class Form1 : Form
    {
        Timer t = new Timer();
        Snake snake;
        Wall wall;
        Food food;
        Graphics g;
        int dir, width, height;

        public Form1()
        {
            InitializeComponent();
            t.Tick += new EventHandler(tick);
            t.Interval = 70;
            t.Enabled = true;
            this.Paint += new PaintEventHandler(draw);
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);
            KeyPreview = false;
            width = this.Size.Width - 20;
            height = this.Size.Height - 50;
            wall = new Wall(width, height);
            snake = new Snake(width, height, wall);
            food = new Food(width, height, wall, snake);
            snake.foodx = food.x;
            snake.foody = food.y;
            dir = 3;
            g = this.CreateGraphics();
        }
        private void refresh()
        {
            g.Clear(Color.AliceBlue);
            Brush b = (Brush)Brushes.Silver;
            foreach (Point p in snake.a)
            {
                if (p.X == snake.x && p.Y == snake.y)
                {
                    g.FillRectangle(Brushes.Black, p.X, p.Y, 10, 10);
                }
                else
                {
                    g.FillRectangle(b, p.X, p.Y, 10, 10);
                }

            }
            g.FillRectangle(Brushes.Red, food.x, food.y, 10, 10);
            foreach (Point p in wall.wall)
            {
                g.FillRectangle(Brushes.DarkGreen, p.X, p.Y, 5, 5);
            }
        }
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void tick(object sender, EventArgs e)
        {
            if (snake.move(dir) == -1)
            {
                t.Enabled = false;
                MessageBox.Show("Game Over");
                restart();
            }
            if (snake.eat() == 1)
            {
                food = new Food(width, height, wall, snake);
                snake.eaten = 1;
                snake.foodx = food.x;
                snake.foody = food.y;
            }
            refresh();
        }
        private void draw(object sender, PaintEventArgs e)
        {
            refresh();
        }
        private void restart()
        {
            wall = new Wall(width, height);
            snake = new Snake(width, height, wall);

            food = new Food(width, height, wall, snake);
            snake.foodx = food.x;
            snake.foody = food.y;
            dir = 1;
            t.Enabled = true;
        }


        private void onKeyUp(object sender, KeyEventArgs e)
        {
            
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up && dir != 3)
            {
                dir = 2;
            }
            else if (e.KeyCode == Keys.Down && dir != 2)
            {
                dir = 3;
            }
            else if (e.KeyCode == Keys.Left && dir != 1)
            {
                dir = 0;
            }
            else if (e.KeyCode == Keys.Right && dir != 0)
            {
                dir = 1;
            }
            if (snake.move(dir) == -1)
            {
                t.Enabled = false;
                MessageBox.Show("Game Over");
                restart();
            }
            if (snake.eat() == 1)
            {
                food = new Food(width, height, wall, snake);
                snake.eaten = 1;
                snake.foodx = food.x;
                snake.foody = food.y;
            }
            refresh();
        }
        
    }
}
