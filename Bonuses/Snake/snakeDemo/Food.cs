﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace snakeDemo
{
    public class Food
    {
        public int x, y;

        public Food(int width, int height, Wall wall, Snake snake)
        {
            Random rnd = new Random();
            bool can = false;
            x = rnd.Next(0, width - 5);
            while (x % 5 != 0)
            {
                x = rnd.Next(0, width - 5);
            }
            y = rnd.Next(0, height - 5);
            while (y % 5 != 0)
            {
                y = rnd.Next(0, height - 5);
            }
            foreach (Point p in wall.wall)
            {
                if (x == p.X && y == p.Y)
                {
                    can = true;
                }
            }
            foreach (Point p in snake.a)
            {
                if (x == p.X && y == p.Y)
                {
                    can = true;
                }
            }
            while (can == true)
            {
                can = false;
                x = rnd.Next(0, width - 5);
                while (x % 5 != 0)
                {
                    x = rnd.Next(0, width - 5);
                }
                y = rnd.Next(0, height - 5);
                while (y % 5 != 0)
                {
                    y = rnd.Next(0, height - 5);
                }
                foreach (Point p in wall.wall)
                {
                    if (x == p.X && y == p.Y)
                    {
                        can = true;
                    }
                }
                foreach (Point p in snake.a)
                {
                    if (x == p.X && y == p.Y)
                    {
                        can = true;
                    }
                }
            }
        }
    }
}
