﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using System.Drawing;

namespace Snake
{
    public class Wall
    {
        public ArrayList wall = new ArrayList();
        public Wall(int width, int height)
        {
            Random RND = new Random();
            int x, y;
            x = RND.Next(0, width - 5);
            while (x % 5 != 0)
            {
                x = RND.Next(0, width - 5);
            }
            y = RND.Next(0, height - 5 * 5);
            while (y % 5 != 0)
            {
                y = RND.Next(0, height - 5 * 15);
            }
            for (int i = 1; i <= 15; i++)
            {
                wall.Add(new Point(x, y)); //создаем стену в произвольном месте
                y += 5;
            }
        }
    }
}
