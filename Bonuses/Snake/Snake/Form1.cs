﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake
{
    public partial class Form1 : Form
    {
        Body body;
        Food food;
        Wall wall;
        Graphics g;
        int w, h, dir;
        public Form1()
        {
            InitializeComponent();
            
            this.Paint += new PaintEventHandler(Draw);
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);
            KeyPreview = false;
            w = 380;
            h = 300;
            wall = new Wall(w, h);
            body = new Body(w, h, wall);
            food = new Food(w, h, wall, body);
            body.xFood = food.x;
            body.yFood = food.y;
            dir = 3;
            g = this.CreateGraphics();
        }
        private void refresh()
        {
            g.Clear(Color.White);
            Brush b = new SolidBrush(Color.IndianRed);
            foreach (Point p in body.MyAL)
            {
                if (p.X == body.x && p.Y == body.y) //если это голова змейки, то закрашиваем в красный
                {
                    g.FillEllipse(Brushes.Red, p.X, p.Y, 10, 10);
                }
                else
                {
                    g.FillEllipse(b, p.X, p.Y, 10, 10); //иначе закрашиваем в черный
                }

            }
            g.FillEllipse(Brushes.DarkBlue, food.x, food.y, 10, 10); //рисуем еду
            foreach (Point p in wall.wall) //рисуем стену
            {
                g.FillRectangle(Brushes.Black, p.X, p.Y, 5, 5);
            }
        }

        private void Draw(object sender, PaintEventArgs e)
        {
            refresh();
        }
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up && dir != 3) //если мы двигаемся вверх, мы не можем двигаться вниз
            {
                dir = 2;
            }
            else if (e.KeyCode == Keys.Down && dir != 2) //если мы двигаемся вниз, мы не можем двигаться вверх
            {
                dir = 3;
            }
            else if (e.KeyCode == Keys.Left && dir != 1) //если мы двигаемся влево, мы не двигаемся вправо
            {
                dir = 0;
            }
            else if (e.KeyCode == Keys.Right && dir != 0) //если мы двигаемся вправо, мы не двигаемся влево
            {
                dir = 1;
            }
            if (body.Move(dir) == -1) //условие проигрыша
            {
                timer1.Enabled = false;
                MessageBox.Show("Game Over");
                restart();
            }
            if (body.eat() == 1)
            {
                food = new Food(w, h, wall, body);
                body.Eat = 1;
                body.xFood = food.x;
                body.yFood = food.y;
            }
            refresh();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (body.Move(dir) == -1) 
            {
                timer1.Enabled = false;
                MessageBox.Show("Game Over");
                restart();
            }
            if (body.eat() == 1)
            {
                food = new Food(w, h, wall, body);
                body.Eat = 1;
                body.xFood = food.x;
                body.yFood = food.y;
            }
            refresh();
        }
        private void restart()
        {
            wall = new Wall(w, h);
            body = new Body(w, h, wall);
            food = new Food(w, h, wall, body);
            body.xFood = food.x;
            body.yFood = food.y;
            dir = 1;
            timer1.Enabled = true;
        }
    }
}
