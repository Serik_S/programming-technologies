﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Collections;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    public class Body
    {
        public int x, y, dir, xFood, yFood, W, H, Eat;
        public Wall wall;
        public ArrayList MyAL; 
        public Body(int _W, int _H, Wall _wall) //Берем длину и ширину поля, Значения стены
        {
            MyAL = new ArrayList();
            x = 0;
            y = 0;
            wall = _wall;
            Eat = 0;
            W = _W;
            H = _H;
            MyAL.Add(new Point(1, 1));
            dir = 3; //движется вниз
        }

        public int Move(int m)
        {
            if (m == 0) //движение влево
            {
                dir = 0;
                x -= 5;
            }
            if (m == 1) //движение вправо
            {
                dir = 1;
                x += 5;
            }
            if (m == 2) //движение вверх
            {
                dir = 2;
                y -= 5;
            }
            if (m == 3) //движение вниз
            {
                dir = 3;
                y += 5;
            }
            foreach (Point p in MyAL) //Если столкнулся сам с собой
            {
                if (x == p.X && y == p.Y)
                {
                    return -1;
                }
            }
            foreach (Point p in wall.wall) //Если столкнулся со стеной, которая находится внутри формы
            {
                if (x == p.X && y == p.Y)
                {
                    return -1;
                }
            }
            if (x < 0 || y < 0 || x > W || y > H) //Если дошел до границы
            {
                return -1;
            }
            if (Eat == 0)   //если он ничего не съел то удаляем хвост
            {
                MyAL.RemoveAt(MyAL.Count - 1);
            }
            else //если он только что съел еду
            {
                Eat = 0; //то обратно обнуляем эту переменную
            }
            MyAL.Insert(0, new Point(x, y));
            return 0;
        }

        public int eat() //функция ЕДА
        {
            if (x  == xFood && y  == yFood) //если он съел еду
            {
                return 1;
            }
            return 0;
        }
    }
}
