﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace Snake
{
    class Food
    {
        public int x, y;

        public Food(int width, int height, Wall wall, Body snake) //берем длину и ширину поля. Берем значения стены и тела
        {
            Random rnd = new Random();
            bool can = true;
            x = rnd.Next(0, width - 5);
            while (x % 5 != 0)  //ищем число кратное 5, потому что змейка ходит по 5 пикселям
            {
                x = rnd.Next(0, width - 5); //произвольно расставляем еду
            }
            y = rnd.Next(0, height - 5);
            while (y % 5 != 0)
            {
                y = rnd.Next(0, height - 5);
            }
            foreach (Point p in wall.wall) //берем все точки стены
            {
                if (x == p.X && y == p.Y) //если координаты еды совпадают с координатами стены
                {
                    can = false;       //то мы не можем туда поставить еду
                }
            }
            foreach (Point p in snake.MyAL) //берем все точки тела
            {
                if (x == p.X && y == p.Y) // если координаты еды совпадают с координатами тела
                {
                    can = false; //то мы не можем поставить туда еду
                }
            }
            while (can == false) //если мы не нашли подходящее место для еды, то мы ищем до тех пор пока не найдем, выполняя те же условия
            {
                can = true;
                x = rnd.Next(0, width - 5);
                while (x % 5 != 0)
                {
                    x = rnd.Next(0, width - 5);
                }
                y = rnd.Next(0, height - 5);
                while (y % 5 != 0)
                {
                    y = rnd.Next(0, height - 5);
                }
                foreach (Point p in wall.wall)
                {
                    if (x == p.X && y == p.Y)
                    {
                        can = false;
                    }
                }
                foreach (Point p in snake.MyAL)
                {
                    if (x == p.X && y == p.Y)
                    {
                        can = false;
                    }
                }
            }
        }
    }
}
