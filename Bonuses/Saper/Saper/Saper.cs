﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saper
{
    class Saper
    {
        public int[,] table;
        int n, m, k;
        public Saper(int _n, int _m, int _k)
        {
            n = _n;
            m = _m;
            k = _k;
            table = new int[n + 2, m + 2];
            bombs();
        }
        public void bombs()
        {
            Random rnd = new Random();
            int x, y;
            for (int i = 1; i <= k; i++)
            {
                x = rnd.Next(1, n);
                y = rnd.Next(1, m);
                table[x, y] = 1;
            }
        }
        public int move(int x, int y)
        {
            if (table[x, y] == 1) return -1;
            else
            {
                int sum = 0;
                for (int i = x - 1; i <= x + 1; i++)
                    for (int j = y - 1; j <= y + 1; j++)
                    {
                        if (table[i, j] == 1) sum++;
                    }
                return sum;
            }
        }
        
    }
}
