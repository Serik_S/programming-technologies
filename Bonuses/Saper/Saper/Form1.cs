﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Saper
{
    public partial class Form1 : Form
    {
        Saper s = new Saper(10, 10, 10);
        MyButton[,] bt = new MyButton[11, 11];
        int flags = 0;
        public Form1()
        {
            InitializeComponent();
            for (int i=1; i<=10; i++)
                for (int j = 1; j <= 10; j++)
                {
                    bt[i,j] = new MyButton(i,j);
                    bt[i, j].MouseDown += new MouseEventHandler(button1_Click);
                    bt[i, j].BackColor = Color.White;
                    bt[i, j].Size = new Size(30, 30);
                    bt[i, j].Location = new Point(i * 30, j * 30);
                    bt[i, j].Text = s.table[i, j].ToString();
                    this.Controls.Add(bt[i, j]);
                }
        }
        void rec(int x, int y)
        {
            bt[x, y].Enabled = false;
            bt[x, y].BackColor = Color.Gray;
            if (s.move(x, y) != 0) 
            { 
               bt[x, y].Text = s.move(x, y).ToString();
            }
            if (s.move(x,y) == 0) { 
             if ( x - 1 > 0 && bt[x - 1,y].BackColor != Color.Gray)
            {
                
                rec(x - 1, y);
            }
            if (y - 1 > 0 && bt[x, y - 1].BackColor != Color.Gray)
            {
                rec(x, y - 1);
            }
            if (x + 1 < 11 && bt[x + 1, y].BackColor != Color.Gray) 
            {
                rec(x + 1, y);
            }
            if (y + 1 < 11 && bt[x, y + 1].BackColor != Color.Gray)
            {
                rec(x, y + 1);
            }
            }
        }
       
        private void button1_Click(object sender, MouseEventArgs e)
        {
            MyButton btns = sender as MyButton;
            int b = s.move(btns.x, btns.y);
            if (e.Button == MouseButtons.Left) 
            { 
                const string message = "You lose. Do you want to start again?";
                
                if (b == -1) btns.Text = "*";
                else if (b == 0)
                {
                    rec(btns.x, btns.y);
                    btns.Text = "";
                }
                else
                {
                    btns.BackColor = Color.Gray;
                    btns.Text = b.ToString();
                    btns.Enabled = false;
                }
                if (btns.Text == "*")
                {
                    DialogResult result1 = MessageBox.Show(message, "important question", MessageBoxButtons.YesNo);
                    if (result1 == DialogResult.Yes) restart();
                    else Close();
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (btns.Enabled == false || flags == 10) return;
                if (btns.Text == "S")
                {
                    btns.Text = "";
                    flags--;
                    return;
                }
                btns.Text = "S";
                flags++;
                int counter = 0;
                if (flags == 10)
                {
                    for (int i = 1; i <= 10; i++)
                        for (int j = 1; j <= 10; j++)
                        {
                            if (bt[i, j].Text == "S" && s.table[i, j] == 1)
                            {
                                counter++;
                            }
                        }

                    if (counter == 10)
                    {
                        MessageBox.Show("You win!");
                    }
                }

            }
        }
        public void restart()
        {
            for (int i = 1; i <= 10; i++)
                for (int j = 1; j <= 10; j++)
                {
                    bt[i, j].Text = "";
                    bt[i, j].BackColor = Color.White;
                    bt[i, j].Enabled = true;
                }
            s = new Saper(10, 10, 10);
        }
        
        private void button1_Click_1(object sender, EventArgs e)
        {
            restart();
        }

    }
}
