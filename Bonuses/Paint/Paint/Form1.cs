﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Paint
{
    public partial class Form1 : Form
    {
        MyButton[,] btns = new MyButton[2, 2];
        int tools = 0;
        Button[,] colors = new Button[2, 4];
        Color[] CL = new Color[8] { Color.Red, Color.Orange, Color.Black, Color.Green, Color.Blue, Color.Yellow, Color.Gray, Color.Violet };
        Pen p = new Pen(Color.Black);

        Bitmap B;
        Graphics Bg;
        Graphics Sg;
        Paint pt = new Paint();
        public Form1()
        {
            InitializeComponent();
            B = new Bitmap(panel1.Width,panel1.Height);
            Bg = Graphics.FromImage(B);
            Sg = panel1.CreateGraphics();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int iterator = 0;
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Button btn = new Button();
                    btn.Text = "";
                    btn.Location = new Point(140 + j * 30, 350 + i * 30);
                    btn.Size = new Size(30, 30);
                    btn.BackColor = CL[iterator];
                    btn.Click += new EventHandler(COLORS);
                    Controls.Add(btn);
                    colors[i, j] = btn;
                    iterator++;
                }
            }
            button5.BackColor = Color.Black;
            Sg.Clear(Color.White);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            pt.n = e.Location;
            pt.Pressed = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            p.Color = button5.BackColor;
        }

        private void COLORS(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            p.Color = btn.BackColor;
            button5.BackColor = btn.BackColor;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tools = 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tools = 2;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tools = 3;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tools = 4;
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            pt.Pressed = false;
            int x = Math.Min(pt.n.X, pt.m.X);
            int y = Math.Min(pt.n.Y, pt.m.Y);
            int w = Math.Abs(pt.n.X - pt.m.X);
            int h = Math.Abs(pt.n.Y - pt.m.Y);

            if (tools == 1)
            {

            }
            if (tools == 2)
            {
                Bg.DrawLine(p, pt.n, pt.m);
            }
            if (tools == 3)
            {
                Bg.DrawRectangle(p, x, y, w, h);
            }
            if (tools == 4)
            {
                Bg.DrawEllipse(p,x,y,w,h);
            }
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            int x = Math.Min(pt.n.X,  pt.m.X); //finding which of 2 points is least (for X coordinates)
            int y = Math.Min(pt.n.Y, pt.m.Y); //finding which of 2 points is least (for Y coordinates) 
            int w = Math.Abs(pt.n.X - pt.m.X); //difference b/w 2 points of X coordinates
            int h = Math.Abs(pt.n.Y - pt.m.Y); // difference b/w 2 points of Y coordinates

            if (pt.Pressed == true) 
            {
                pt.m = e.Location;
                if (tools == 1)
                {
                    Sg.DrawImage(B, 0, 0);
                    Sg.DrawLine(p, pt.n, pt.m);
                    Bg.DrawLine(p, pt.n, pt.m);
                    pt.n = pt.m;
                }
                if (tools == 2)
                {
                    Sg.Clear(Color.White);
                    Sg.DrawImage(B, 0, 0);
                    Sg.DrawLine(p, pt.n, pt.m);
                }
                if (tools == 3)
                {
                    Sg.Clear(Color.White);
                    Sg.DrawImage(B, 0, 0);
                    Sg.DrawRectangle(p, x, y, w, h);
                }
                if (tools == 4)
                {
                    Sg.Clear(Color.White);
                    Sg.DrawImage(B, 0, 0);
                    Sg.DrawEllipse(p, x, y, w, h);
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Sg.Clear(Color.White);
            B = new Bitmap(panel1.Width, panel1.Height);
            Bg = Graphics.FromImage(B);
            Sg = panel1.CreateGraphics();
        }

    }
}
