﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab2_1
{
    class Program
    {
        static void ShowDirectory(DirectoryInfo dir, int depth = 0)
        {

            foreach (DirectoryInfo subDir in dir.GetDirectories())
            {
                for (int i = 1; i <= depth; i++)
                    Console.Write(" ");
                Console.WriteLine(subDir + "/");
                ShowDirectory(subDir, depth + 1);
            }
        }
        static void Main(string[] args)
        {
            DirectoryInfo dir = new DirectoryInfo(@"C:\lab_works");
            ShowDirectory(dir);
            Console.ReadKey();
        }
    }
}
