﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovingBall
{
    class Ball
    {
        public int x, y, r;
        public Ball(int _x, int _y, int _r)
        {
            x = _x;
            y = _y;
            r = _r;
        }
    }
}
