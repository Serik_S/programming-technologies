﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace isPhone
{
    class Program
    {
        static string ReformatPhone(string s)
        {
            Match m = Regex.Match(s, @"^\(?(\d{3})\)?[\s\-]?(\d{3})\-?(\d{4})$");
            return String.Format("({0}) {1}-{2}", m.Groups[1], m.Groups[2], m.Groups[3]);
        }

        static bool IsZip(string s)
        {
            return Regex.IsMatch(s, @"^\d{5}(\-\d{4})?$");
        }

        static bool IsPhone(string s)
        {
            return Regex.IsMatch(s, @"^\(?\d{3}\)?[\s\-]?\d{3}\-?\d{4}$");
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter:");
            string s = Console.ReadLine();
            foreach (string s in input)
            {
                 if (IsPhone(s)==true) Console.WriteLine(ReformatPhone(s) + " is a phone number");
                 else if (IsZip(s)==true) Console.WriteLine(s + " is a zip code");
                 else Console.WriteLine(s + " is unknown");
           }
            Console.ReadKey();
        }
    }
}



