﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace stringJoin
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> a = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                a.Add("boo");
            }
            string s = string.Join(" ", a);
            Console.WriteLine(s);
            Console.ReadKey();
        }
    }
}
