﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RectangleOrEllipse
{
    public partial class Form1 : Form
    {
        int x = 20;
        int n = 200;
        int m = 90;
        int c = 1;
        int figure = 0;
        Graphics g;
        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (figure == 1) 
            { 
                if (c==1)   
                {
                    g = this.CreateGraphics();
                    Rectangle rec = new Rectangle(n -= 2, m -= 2, x += 2, x += 2);
                    Pen mypen = new Pen(Color.Green, 2);
                    g.DrawEllipse(mypen, rec);
                    c++;
                }
                else if (c == 2)
                {
                    g = this.CreateGraphics();
                    Rectangle rec = new Rectangle(n -= 2, m -= 2, x += 2, x += 2);
                    Pen mypen = new Pen(Color.Yellow, 2);
                    g.DrawEllipse(mypen, rec);
                    c++;
                }
                else if (c == 3)
                {
                    g = this.CreateGraphics();
                    Rectangle rec = new Rectangle(n -= 2, m -= 2, x += 2, x += 2);
                    Pen mypen = new Pen(Color.Red, 2);
                    g.DrawEllipse(mypen, rec);
                    c = 1;
                }
            }
            else if (figure == 2)
            {
                if (c == 1)
                {
                    g = this.CreateGraphics();
                    Rectangle rec = new Rectangle(n -= 2, m -= 2, x += 2, x += 2);
                    Pen mypen = new Pen(Color.Green, 2);
                    g.DrawRectangle(mypen, rec);
                    c++;
                }
                else if (c == 2)
                {
                    g = this.CreateGraphics();
                    Rectangle rec = new Rectangle(n -= 2, m -= 2, x += 2, x += 2);
                    Pen mypen = new Pen(Color.Yellow, 2);
                    g.DrawRectangle(mypen, rec);
                    c++;
                }
                else if (c == 3)
                {
                    g = this.CreateGraphics();
                    Rectangle rec = new Rectangle(n -= 2, m -= 2, x += 2, x += 2);
                    Pen mypen = new Pen(Color.Red, 2);
                    g.DrawRectangle(mypen, rec);
                    c = 1;
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            figure = 2;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            figure = 1;
        }
    }
}
