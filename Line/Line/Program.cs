﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point
{
    class Point
    {
        public int x, y;
        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;
        }
    }
    class Line
    {
        public Point n;
        public Point m;

        public Line(Point _n, Point _m)
        {
            n = _n;
            m = _m;
        }
        public override string ToString()
        {
            double dist = Math.Sqrt((n.x - m.x) * (n.x - m.x) + (n.y - m.y) * (n.y - m.y));
            return dist.ToString();
        }


    }
    class Program
    {
        static void Main(string[] args)
        {
            Point a = new Point(1, 2);
            Point b = new Point(3, 4);
            Line l = new Line(a, b);
            
            Console.WriteLine(l);
            Console.ReadKey();

        }
    }
}
